const app = document.getElementById('root')

const container = document.createElement('div')
container.setAttribute('class', 'container')

app.appendChild(container)

const request = new XMLHttpRequest()

request.open('GET', 'https://ghibliapi.herokuapp.com/films', true)

request.onload = function() {

  let data = JSON.parse(this.response)

  // Permet de connaitre le nombre de films

  let i = 0;

  if (request.status >= 200 && request.status < 400) {


    data.forEach(film => {

      i++;

      // Crée les cartes
      const card = document.createElement('div')
      card.setAttribute('class', 'card')
      card.setAttribute('id', i)

      // Permet d'afficher le titre
      const title = document.createElement('h1')
      title.textContent = film.title

      // Permet d'afficher la description
      const description = document.createElement('p')
      description.textContent = `${film.description.substring(0, 500)}...`

      //Afficher les élements

      container.appendChild(card)
      card.appendChild(title)
      card.appendChild(description)

      console.log(film.title)

    })
  } else {


    const errorMessage = document.createElement('marquee')

    errorMessage.textContent = "Erreur"

    //Container caché,
    container.appendChild(errorMessage)

    /*Pour voir l'erreur :
    Ou voir la console.
    */

    //conteneur.appendChile(errorMessage)

    console.log(errorMessage.textContent)
  }
}


request.send()